#define _GNU_SOURCE

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <glib.h>

#include <gst/gst.h>
#include <gst/app/gstappsrc.h>

#include <mumbleclient/mumble.h>

typedef struct _User
{
    const MumbleUser *user;
    GstAppSrc *appsrc;
} User;

gint compare_User_by_session(gconstpointer a, gconstpointer b)
{
    User *user = (User*)a;
    uint32_t *session = (uint32_t*)b;

    return !(user->user->session == *session);
}


typedef struct _User_callback_data
{
    GList *user_list;
    GstPipeline *pipeline;
    GstElement *mixer;
    GstElement *opusdec;
    GstElement *opusparse;
    GstElement *appsrc;
    int to_quit;
} User_callback_data;


void audio_callback(uint8_t *opus, const size_t opus_header, const MumbleUser *user, void *data)
{
    User_callback_data *cb_data = (User_callback_data*)data;


    GList *found_user_list = g_list_find_custom(cb_data->user_list, (gconstpointer)&user->session, compare_User_by_session);
    if (!found_user_list) {
        fprintf(stderr, "User not in list.\n");
        return;
    }
    else {
        printf("User found\n");
    }

    User* found_user = (User*)found_user_list->data;

    //gst_element_set_state(GST_ELEMENT(found_user->appsrc), GST_STATE_PLAYING);
    
    GstBuffer *buffer = gst_buffer_new_allocate(NULL, opus_header & 0x1fff, NULL);
    if (!buffer) {
            fprintf(stderr, "Could not allocated GstBuffer\n");
            return;
    }
    gsize bytes_written = gst_buffer_fill(buffer, 0, opus, opus_header & 0x1fff);
    if (bytes_written < (opus_header & 0x1fff)) {
            fprintf(stderr, "Not all bytes were written\n");
    }
    GstFlowReturn ret = gst_app_src_push_buffer(found_user->appsrc, buffer);
    switch (ret) {
    case GST_FLOW_OK:
            printf("GST_FLOW_OK\n");
            break;
    case GST_FLOW_FLUSHING:
            fprintf(stderr, "GST_FLOW_FLUSHING\n");
            break;
    case GST_FLOW_EOS:
            fprintf(stderr, "GST_FLOW_EOS\n");
            break;
    default:
            fprintf(stderr, "Unknown return value\n");
            break;
    }

    // teminator bit
    //if (opus_header & 0x2000) {
    //}
}

void user_callback(const MumbleUser *user, void *data, bool is_new)
{
    User_callback_data *cb_data = (User_callback_data*)data;
    if (is_new) {
        printf("New user connected\n");
        User *user_to_add = calloc(1, sizeof(User));
        if(!user) return;

        user_to_add->user = user;

        user_to_add->appsrc = GST_APP_SRC(gst_element_factory_make("appsrc", NULL));
        GstElement *opusparse = gst_element_factory_make("opusparse", NULL);
        GstElement *opusdec = gst_element_factory_make("opusdec", NULL);

        if(!user_to_add->appsrc || !opusparse || !opusdec) {
            printf("Not all elements could be created for user.\n");
            return;
        }

        // configure appsrc to properly resume from stream
        g_object_set(user_to_add->appsrc, "do-timestamp", true, NULL);
        g_object_set(user_to_add->appsrc, "is-live", true, "format", GST_FORMAT_TIME, NULL);
        
        gst_bin_add_many(GST_BIN(cb_data->pipeline), GST_ELEMENT(user_to_add->appsrc), opusparse, opusdec, NULL);
 
        if (!gst_element_link(GST_ELEMENT(user_to_add->appsrc), opusparse)) {
            fprintf(stderr, "appsrc and opusparse could not be linked.\n");
            return;
        }
        if (!gst_element_link(opusparse, opusdec)) {
            fprintf(stderr, "opusparse and opusdec could not be linked.\n");
            return;
        }
        if (!gst_element_link(opusdec, cb_data->mixer)) {
            fprintf(stderr, "opusdec and mixer could not be linked.\n");
            return;
        }


        
        //
        //GstPad *opusdec_pad = gst_element_get_static_pad(opusdec, "src");
        //GstPad *mixer_pad = gst_element_get_compatible_pad(cb_data->mixer, opusdec_pad, NULL);
        //gst_pad_link(opusdec_pad, mixer_pad);
        //gst_object_unref(GST_OBJECT(opusdec_pad));
        //gst_object_unref(GST_OBJECT(mixer_pad));
        //g_object_set(user_to_add->appsrc, "is-live", true, NULL);
        ////g_object_set(user_to_add->appsrc, "do-timestamp", true, NULL);

        //
        GstStateChangeReturn ret;
        ret = gst_element_set_state(GST_ELEMENT(user_to_add->appsrc), GST_STATE_PLAYING);
        ret = gst_element_set_state(opusparse, GST_STATE_PLAYING);
        ret = gst_element_set_state(opusdec, GST_STATE_PLAYING);
        GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(cb_data->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "new_user");

        cb_data->user_list = g_list_append(cb_data->user_list, user_to_add);
    }
}

int main(int argc, char** argv) {

    gst_init(NULL, NULL);

    User_callback_data *cb_data = calloc(1, sizeof(User_callback_data));
    if (!cb_data) {
        fprintf(stderr, "Could not allocated memory\n");
    }

    cb_data->pipeline = GST_PIPELINE(gst_pipeline_new("audio-playback"));
    cb_data->appsrc = gst_element_factory_make("appsrc", "appsrc");
    cb_data->opusparse = gst_element_factory_make("opusparse", NULL);
    cb_data->opusdec = gst_element_factory_make("opusdec", "opusdec");
    cb_data->mixer = gst_element_factory_make("audiomixer", "audiomixer");
    GstElement *audio_sink = gst_element_factory_make("autoaudiosink", "audio_sink");

    if (!cb_data->pipeline) {
        fprintf(stderr, "Pipeline could not be created\n");
        return -1;
    }
    
    if (!cb_data->appsrc || !cb_data->opusparse || !cb_data->opusdec || !cb_data->mixer || !audio_sink) {
        fprintf(stderr, "Not all elements could be created.\n");
        return -1;
    }

    // set name for pulseaudio to see (not working atm)
    g_object_set(audio_sink, "name", "libmumbleclient", NULL);

    // configure cb_data->appsrc to properly resume from stream
    g_object_set(cb_data->appsrc, "do-timestamp", true, NULL);
    g_object_set(cb_data->appsrc, "is-live", true, "format", GST_FORMAT_TIME, NULL);

    gst_bin_add_many(GST_BIN(cb_data->pipeline), cb_data->appsrc, cb_data->opusparse, cb_data->opusdec, cb_data->mixer, audio_sink, NULL);
    //gst_bin_add_many(GST_BIN(cb_data->pipeline), cb_data->mixer, audio_sink, NULL);

    if (!gst_element_link(cb_data->appsrc, cb_data->opusparse)) {
        printf("cb_data->appsrc and cb_data->opusparse could not be linked\n");
        return -1;
    }

    if (!gst_element_link(cb_data->opusparse, cb_data->opusdec)) {
        printf("cb_data->opusparse and cb_data->opusdec could not be linked\n");
        return -1;
    }
    
    if (!gst_element_link(cb_data->opusdec, cb_data->mixer)) {
        printf("cb_data->opusdec and mixer could not be linked\n");
        return -1;
    }
    
    if (!gst_element_link(cb_data->mixer, audio_sink)) {
        printf("mixer and audiosink could not be linked\n");
        return -1;
    }

    
    
    GstStateChangeReturn ret = gst_element_set_state(GST_BIN(cb_data->pipeline), GST_STATE_PLAYING);
    //ret = gst_element_set_state(cb_data->mixer, GST_STATE_PLAYING);
    //ret = gst_element_set_state(audio_sink, GST_STATE_PAUSED);

    //GstState state;
    //GstState pending;

    //ret = gst_element_get_state(audio_sink, &state, &pending, GST_CLOCK_TIME_NONE);
    //printf("%s\n", gst_element_state_get_name(state));
    //
    //switch (ret) {
    //    case GST_STATE_CHANGE_FAILURE:
    //        printf("GST_STATE_CHANGE_FAILURE:\n");
    //        break;
    //    case GST_STATE_CHANGE_SUCCESS:
    //        printf("GST_STATE_CHANGE_SUCCESS:\n");
    //        break;
    //    case GST_STATE_CHANGE_ASYNC:
    //        printf("GST_STATE_CHANGE_ASYNC:\n");
    //        break;
    //    case GST_STATE_CHANGE_NO_PREROLL:
    //        printf("GST_STATE_CHANGE_NO_PREROLL:\n");
    //        break;
    //}

    //GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(cb_data->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline_base");
    
    MumbleClient client;
    client = MumbleClient_create("ohea.xyz", 64738);
    MumbleClient_set_audio_callback(client, audio_callback, (void*)cb_data);
    MumbleClient_set_user_callback(client, user_callback, (void*)cb_data);
    if (MumbleClient_connect(client) < 0) {
        printf("Error connecting to server...\n");
        printf("Exiting.....\n");
        return 1;
    }
}
