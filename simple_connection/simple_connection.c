#include <stdio.h>

#include <mumbleclient/mumble.h>


int main(int argc, char** argv) {
    MumbleClient client;
    client = MumbleClient_create("ohea.xyz", 64738);
    if (MumbleClient_connect(client) < 0) {
        printf("Error connecting to server...\n");
        printf("Exiting.....\n");
        return 1;
    }
}
